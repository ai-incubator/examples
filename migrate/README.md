# Migrating to Dialogflow Example Code

### Environment Setup
Set up Google Cloud Platform credentials and install dependencies.
```sh
gcloud auth login
gcloud auth application-default login
gcloud config set project <project name>
```
```sh
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

### Usage
1) Get cx agent id. You can get this from the url or using api or from the agent selection dashboard from the 3 dot menu of your agent select Copy Name.
1) Get es agent id. This is just a name. It doesn't look like the path that CX does.
1) Note that client api endpoint used is based on location of CX agent
1) Replace your values in command and run in venv
```
CX_AGENT_ID=projects/-/locations/-/agents/-
ES_AGENT_ID=my-dialogflow-project-name
python3 migrate.py \
--cx_agent_id $CX_AGENT_ID \
--es_project_id $ES_AGENT_ID
```