import argparse
import google
import logging
import time

from google.cloud import dialogflow
from google.cloud.dialogflowcx_v3.services.entity_types import EntityTypesClient
from google.cloud.dialogflowcx_v3.services.flows import FlowsClient
from google.cloud.dialogflowcx_v3.services.intents import IntentsClient
from google.cloud.dialogflowcx_v3.types import CreateIntentRequest
from google.cloud.dialogflowcx_v3.types import EntityType
from google.cloud.dialogflowcx_v3.types import TransitionRoute
from google.cloud.dialogflowcx_v3.types.intent import Intent
from google.protobuf.field_mask_pb2 import FieldMask

_ENTITY_MAPPING = {
    "sys.geo-city-us": "sys.geo-city",
    "sys.geo-state-us": "sys.geo-state",
    "sys.geo-county-us": "sys.geo-county",
    "sys.geo-city-gb": "sys.geo-city",
    "sys.geo-state-gb": "sys.geo-state",
    "sys.geo-county-gb": "sys.geo-county",
    "sys.street-address": "sys.location",
    "sys.duration": "sys.duration",
    "sys.percentage": "sys.percentage",
    "sys.temperature": "sys.temperature",
    "sys.unit-currency": "sys.unit-currency",
    "sys.email": "sys.email",
    "sys.phone-number": "sys.phone-number",
    "sys.date": "sys.date",
    "sys.date-period": "sys.date-period",
    "sys.date-time": "sys.date-time",
    "sys.time": "sys.time",
    "sys.time-period": "sys.time-period",
    "sys.any": "sys.any",
    "sys.url": "sys.url",
    "sys.address": "sys.address",
    "sys.geo-city": "sys.geo-city",
    "sys.geo-country": "sys.geo-country",
    "sys.geo-state": "sys.geo-state",
    "sys.location": "sys.location",
    "sys.zip-code": "sys.zip-code",
    "sys.given-name": "sys.given-name",
    "sys.last-name": "sys.last-name",
    "sys.person": "sys.person",
    "sys.cardinal": "sys.cardinal",
    "sys.flight-number": "sys.flight-number",
    "sys.number": "sys.number",
    "sys.number-integer": "sys.number-integer",
    "sys.number-sequence": "sys.number-sequence",
    "sys.ordinal": "sys.ordinal",
    "sys.color": "sys.color",
    "sys.language": "sys.language",
    "sys.currency-name": "sys.currency-name"
}
_DEFAULT_ENTITY_TYPE = "sys.any"

# logging config
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


class ESToCXMigrator:
  """Class to migrate ES agents to CX. Suggested to migrate entities first to a brand new agent.
  Supports Migrating:
  - intents, training phrases,
  - entities, synonyms
  - parent transition route groups
  - Default language only
  """

  def __init__(self, cx_agent_id, es_project_id, client_api_endpoint):
    self.client_options = {"api_endpoint": client_api_endpoint}
    self.es_project_id = es_project_id
    self.cx_agent_id = cx_agent_id

  def migrate_intents(self):
    intents = self.get_intents_es()
    cx_entities = self.get_entity_types_cx()
    cx_entities_dict = {x.display_name: x for x in cx_entities}

    for intent in intents:
      logging.info("Migrating intent: {}".format(intent.display_name))
      logging.info("# parameters: {}".format(len(intent.parameters)))
      logging.info("# trainingphrases: {}".format(len(intent.training_phrases)))

      if len(intent.parameters) > 19:
        logging.warning(
            "{} intent has more than 19 parameters. Ignoring...".format(
                intent.display_name))
        continue

      training_phrases = []
      parameters = {}
      for training_phrase in intent.training_phrases:
        parts = []
        for part in training_phrase.parts:
          # Case where Is annotated training phrase
          if part.alias:
            parts.append(
                Intent.TrainingPhrase.Part(
                    text=part.text, parameter_id=part.alias))
            entity_type = part.entity_type[1:]

            # Assuming that all custom entities already exist in CX agent
            if entity_type in cx_entities_dict:
              parameters[part.alias] = Intent.Parameter(
                  id=part.alias, entity_type=cx_entities_dict[entity_type].name)

            else:  # Assuming sys entity

              # Some sys entities type has changed is CX
              entity_type = self.map_system_entitytype(entity_type)

              parameters[part.alias] = Intent.Parameter(
                  id=part.alias,
                  entity_type="projects/-/locations/-/agents/-/entityTypes/" +
                              entity_type)

          # Case where is not annotated trainingphrase
          else:
            parts.append(Intent.TrainingPhrase.Part(text=part.text))

        training_phrases.append(
            Intent.TrainingPhrase(parts=parts, repeat_count=1))

      intent_cx = Intent(
          display_name=intent.display_name,
          training_phrases=training_phrases,
          parameters=list(parameters.values()))

      self.create_intent_cx(intent_cx)

      # prevent resource exception
      time.sleep(1)

  def migrate_entity_types(self):
    entity_types = self.get_entity_types_es()

    for entity_type in entity_types:
      cx_entity_type = self.map_custom_entitytype(entity_type)
      self.create_entity_type_cx(cx_entity_type)

      # to avoid resource exception
      time.sleep(1)

  def create_entity_type_cx(self, cx_entity_type):
    entity_client = EntityTypesClient(client_options=self.client_options)
    try:
      entity_client.create_entity_type(entity_type=cx_entity_type,
                                       parent=self.cx_agent_id)
    except google.api_core.exceptions.AlreadyExists:
      logging.warning("EntityType already exists: {}".format(
                      cx_entity_type.display_name))
      pass

  def create_intent_cx(self, intent):
    intents_client_cx = IntentsClient(client_options=self.client_options)
    try:
      intent_request = CreateIntentRequest(intent=intent,
                                           parent=self.cx_agent_id)
      intents_client_cx.create_intent(request=intent_request)
    except google.api_core.exceptions.AlreadyExists:
      logging.warning("Intent already exists: {}".format(
                      intent.display_name))
      pass

  def get_intents_es(self):
    intents_client = dialogflow.IntentsClient()
    parent = dialogflow.AgentsClient.agent_path(self.es_project_id)
    intents_pager = intents_client.list_intents(request={
        "parent": parent,
        "intent_view": 1
    })

    intents = []
    for i in intents_pager.__iter__():
      intents.append(i)

    logging.info("# Available ES Intents: {}".format(len(intents)))

    return intents

  def get_entity_types_cx(self):
    entity_client = EntityTypesClient(client_options=self.client_options)
    entity_types_pager = entity_client.list_entity_types(
        parent=self.cx_agent_id)
    entities = []

    for e in entity_types_pager.__iter__():
      entities.append(e)

    logging.info("# CX EntityTypes: {}".format(len(entities)))

    return entities

  def get_intents_cx(self):
    intents_client = IntentsClient(client_options=self.client_options)
    intents_pager = intents_client.list_intents(parent=self.cx_agent_id)
    intents = []

    for i in intents_pager.__iter__():
      intents.append(i)

    logging.info("Total CX Intents: {}".format(len(intents)))
    return intents

  def get_flow_cx(self, default_flow_name):
    flows_client = FlowsClient(client_options=self.client_options)
    flows_pager = flows_client.list_flows(parent=self.cx_agent_id)

    for flow in flows_pager.flows:
      if flow.display_name == default_flow_name:
        return flow

  def update_flow(self):
    logging.info("Adding all es parent intents as route to default flow")
    flows_client = FlowsClient(client_options=self.client_options)
    default_flow = self.get_flow_cx("Default Start Flow")
    cx_intents = self.get_intents_cx()
    es_intents = self.get_intents_es()
    # es_parent_intents.append("Default Welcome Intent")

    es_parent_intents = []
    for intent in es_intents:
      if ((intent.parent_followup_intent_name is None) or (
          not intent.parent_followup_intent_name)):
        es_parent_intents.append(intent.display_name)

    routes = []
    for intent in cx_intents:

      if not intent.is_fallback and intent.display_name in es_parent_intents:

        logging.info(
            "Adding intent to default flow: {}".format(intent.display_name))
        routes.append(
            TransitionRoute(intent=intent.name, trigger_fulfillment={}))

      else:
        logging.info(
            "Skipping {} intent from default flow".format(intent.display_name))

    default_flow.transition_routes = routes

    flows_client.update_flow(
        flow=default_flow, update_mask=FieldMask(paths=["transition_routes"]))

  def get_entity_types_es(self):
    entity_types_client = dialogflow.EntityTypesClient()
    parent = dialogflow.AgentsClient.agent_path(self.es_project_id)
    entity_types = entity_types_client.list_entity_types(
        request={"parent": parent})

    entities = []
    for entity_type in entity_types.__iter__():
      entities.append(entity_type)
    logging.info("# Available ES EntityTypes: {}".format(len(entities)))

    return entities

  def map_system_entitytype(self, entity_type: str):
    if not entity_type in _ENTITY_MAPPING.keys():
      logging.warning(
          "Unknown System entity type : {}. Mapping to sys.any".format(
              entity_type))

    mapped_type = _ENTITY_MAPPING.get(entity_type, _DEFAULT_ENTITY_TYPE)

    if entity_type != mapped_type:
      logging.warning(
          "EntityType Mapped. {} --> {}".format(entity_type, mapped_type))
    return mapped_type

  def map_custom_entitytype(self, entity_type: EntityType):

    if "sys." in entity_type.display_name:
      return self.map_system_entitytype(entity_type.display_name)

    entity_type.name = None

    cx_entities = []
    for entity in entity_type.entities:
      cx_synonyms = []
      for synonym in entity.synonyms:
        cx_synonyms.append(synonym)

      cx_entities.append(
          EntityType.Entity(value=entity.value, synonyms=cx_synonyms))

    return EntityType(
        display_name=entity_type.display_name,
        kind=entity_type.kind,
        entities=cx_entities,
        enable_fuzzy_extraction=entity_type.enable_fuzzy_extraction)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
      description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
  required = parser.add_argument_group('required arguments')
  required.add_argument("--cx_agent_id", type=str,
                      help="CX agent path. It has the form: projects/*/locations/*/agents/*****-******-******-****", required=True)
  required.add_argument("--es_project_id", type=str, help="ES project name.", required=True)

  args = parser.parse_args()
  location = args.cx_agent_id.split("/")[3]
  api_endpoint = "{}-dialogflow.googleapis.com:443".format(location)

  logging.info("Using {}".format(api_endpoint))

  m = ESToCXMigrator(args.cx_agent_id, args.es_project_id, api_endpoint)
  m.migrate_entity_types()
  m.migrate_intents()
  m.update_flow()
