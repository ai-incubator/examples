[ -z "$1" ] && echo "Please provide a fieldmask that matches fields in request.json e.g. languageCode" && exit
FIELDMASK=$1
source .env
curl -X PATCH \
  -H "Authorization: Bearer "$(gcloud auth --impersonate-service-account=$SA@$PROJECT.iam.gserviceaccount.com print-access-token) \
  -H "Content-Type: application/json; charset=utf-8" \
  -d @request.json \
  https://dialogflow.googleapis.com/v2beta1/projects/$PROJECT/conversationProfiles/$INTEGRATION_ID?updateMask=$FIELDMASK

