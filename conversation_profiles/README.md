# Managing Conversation Profile Example code

The following commands all depends on .env file to be neat. Please update values
and source them.

## To update conversation profile
1) Update `.env` with your project name, user email, service account name (you might need to create one steps below), and the integration ID of the conversation profile you want to change.
1) Update request.json with values
1) Run update script with fieldmask. For example for language use follow command. 
See https://developers.googleblog.com/2017/04/using-field-masks-with-google-apis-for.html for more info.
   ```
   ./update.sh languageCode
   ```


## Prereqs - Setup service account impersonation.
The following are suggested commands to set this up. Your mileage may vary.


1) Authenticate yourself
   ```
   gcloud auth login
   ```

1) Create service account, if you don't already have one.
   ```
   gcloud iam service-accounts create $SA \
    --description="Test service account for manually updating conversation profiles" \
    --display-name="$SA"
   ```
1) Make sure your user has roles/iam.serviceAccountTokenCreator on the service account
    ```
    gcloud iam service-accounts add-iam-policy-binding $SA@$PROJECT.iam.gserviceaccount.com \
    --member="user:$EMAIL" --role="roles/iam.serviceAccountTokenCreator" --project=$PROJECT
    ```

2) Check impersonation is working 
   ```
   gcloud services enable iamcredentials.googleapis.com;
   gcloud auth --impersonate-service-account=$SA@$PROJECT.iam.gserviceaccount.com print-access-token
   ```

3) Your service account needs roles/dialogflow.conversationManager
   ```
   gcloud projects add-iam-policy-binding $PROJECT \
    --member=serviceAccount:$SA@$PROJECT.iam.gserviceaccount.com \
    --role=roles/dialogflow.conversationManager
   ```


